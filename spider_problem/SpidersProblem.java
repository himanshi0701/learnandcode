import java.util.*;

/*
SpidersProblem is a class which selects a number of spiders from the list of 
the given spiders,dequeue the power of those selected spiders.The list
represents the power of each spider. The position of the spider with maximum power is
displayed as output, and the remaining spiders are enqueued back to the initial list 
by decrementing their power by one unit till it is zero.  
 */

class SpidersProblem {
	
	
    public static class SpiderInformation {
        //Position of spiders
    	int position;
        //Power associated with each spider
    	int power;
        public SpiderInformation(int place,int strength) {
            position = place;
            power = strength;
        }
	}
	
	/*SpiderInformation is the node of the linked list which stores power and position
	of spiders in the linked list
	*/
	LinkedList<SpiderInformation> spiderQueue = new LinkedList<SpiderInformation>();
	SpiderInformation newSpiderInformation;
	int selectSpiders;
	int spiders;

	public void getData() {
		Scanner getinput = new Scanner(System.in);
		System.out.println("Enter the number of spiders in the list: ");
		spiders = getinput.nextInt();
		System.out.println("Enter the number of spiders to be selected to dequeue from the list : ");
		selectSpiders = getinput.nextInt();	
		/*
		 This loop takes the power of each spider in the linkedlist
		*/
		System.out.println("Enter the power of each spider: ");
		for(int index = 1 ; index <= spiders ; index++) {
			newSpiderInformation = new SpiderInformation(index , getinput.nextInt());
			spiderQueue.addLast(newSpiderInformation);
		}
	}
	
	public void displaySelectedSpiders() {
		/*
		 This loop is used to dequeue the spiders from the list, finds the position
		 of the spider with maximum power and display that position, eliminates that
		 spider from the list. Remaining spiders are enqueued back with their 
		 power decremented by one(till zero).
		 */
		System.out.println("Positions of selected spider's in the order they are selected");
		for(int spiderDequeueIndex = 0 ; spiderDequeueIndex < selectSpiders ; spiderDequeueIndex++) {
			int temporaryPosition = 0;
			int dequeueSpider = Math.min(selectSpiders , spiderQueue.size());
			for(int selectedSpiderIndex = 1 ; selectedSpiderIndex < dequeueSpider ; selectedSpiderIndex++) {
				if(spiderQueue.get(selectedSpiderIndex).power > spiderQueue.get(temporaryPosition).power) 
				temporaryPosition = selectedSpiderIndex;
			}
			if(spiderDequeueIndex == selectSpiders-1) {
				System.out.println(spiderQueue.get(temporaryPosition).position);
				break;
			}
			else System.out.print(spiderQueue.get(temporaryPosition).position+" ");
			spiderQueue.remove(temporaryPosition);
			dequeueSpider--;
			while(dequeueSpider > 0) {
				newSpiderInformation = spiderQueue.removeFirst();
				if(newSpiderInformation.power > 0) newSpiderInformation.power--;
				spiderQueue.addLast(newSpiderInformation);
				dequeueSpider--;
			}	
		}
	}

    public static void main(String args[] ) throws Exception {
    	SpidersProblem spiderProblem = new SpidersProblem();
		spiderProblem.getData();
		spiderProblem.displaySelectedSpiders();
    }
}
