package mydb;

import java.io.IOException;
import java.util.List;

import customexception.MyDbExceptions;

public interface DBInterface {
		public <T> void save(T object) throws IOException, MyDbExceptions;

		public <T> List<T> find(T object, String searchKey, String searchValue)
				throws IOException, MyDbExceptions;

		public <T> int delete(T object, String deleteKey, String deleteValue)
				throws IOException, MyDbExceptions;

		public <T> int modify(T object, String id) throws IOException, MyDbExceptions;
	}
