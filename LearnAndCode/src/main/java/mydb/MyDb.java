package mydb;

import java.io.IOException;
import java.util.List;

import customexception.MyDbExceptions;
import dbuser.Employee;
import services.MyDbMethods;

public class MyDb implements DBInterface {
	public <T> void save(T object) throws IOException, MyDbExceptions {
		MyDbMethods.insert(object);
	}

	public <T> List<T> find(T object, String searchKey, String searchValue)
			throws IOException, MyDbExceptions {

		return MyDbMethods.search(searchKey, searchValue, object);
	}

	public <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, MyDbExceptions {

		return MyDbMethods.delete(object, deleteKey, deleteValue);
	}

	public <T> int modify(T object, String id) throws IOException, MyDbExceptions {
		return MyDbMethods.update(object, id);
	}
}
