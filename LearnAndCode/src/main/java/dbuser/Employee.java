package dbuser;

public class Employee {
	public String id;
	public String name;
	public String designation;

	public Employee() {

	}

	public Employee(String name, String designation) {
		super();
		this.name = name;
		this.designation = designation;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Employee [Id = " + id + ", Name = " + name + ", Designation = " + designation + "]";
	}
}