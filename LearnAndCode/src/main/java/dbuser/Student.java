package dbuser;

public class Student {

	public String id;
	public String name;
	public String address;

	Student() {

	}

	public Student(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Student [Id = " + id + ", Name = " + name + ", Address = " + address + "]";
	}
}
