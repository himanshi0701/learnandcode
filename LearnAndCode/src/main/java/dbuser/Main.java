package dbuser;

import java.io.IOException;
import java.util.List;
import customexception.MyDbExceptions;
import mydb.MyDb;

public class Main {

static Student student1,student2,student3;
static Employee employee1,employee2,employee3;

public static void main(String args[]) throws IOException, MyDbExceptions {
		MyDb db = new MyDb();
		
//		insert objects into database
		student1 = new Student("Sam", "Delhi");
		db.save(student1);
		student2 = new Student("Ed","Gurgaon");
		db.save(student2);
		student3 = new Student("steve","Mumbai");
		db.save(student3);
		employee1 = new Employee("Rita", "Software Engineer");
		db.save(employee1);
		employee2 = new Employee("Geeta", "Software Engineer");
		db.save(employee2);
		employee3 = new Employee("Rashmi", "Manager");
		db.save(employee3);
		

//		search objects into database
		List<Employee> employee = db.find(employee1, "name", "Rita");
		System.out.println(employee);

//		delete objects from database
//		int rowsAffected = db.delete(student1, "id", "cf9b0a03-8786-460b-9e5e-a6339875aeb0");
//		System.out.println(rowsAffected);
		
//		update objects into database
		student2.setName("Donald");
		student2.setAddress("UK");
//		int rowsUpdated = db.modify(student2, "e0380044-2490-4884-acef-2d008fb923f5");
//		System.out.println(rowsAffected);
		
            }		
}