package services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

public class FileUtil {
	public static <T> String getFilePath(T object) {
		return System.getProperty("user.dir") + "//ITTDB_FileStore//" + object.getClass().getSimpleName() + ".json";
	}

	public static boolean isPresent(String filePath) {
		return (Files.exists(Paths.get(filePath), LinkOption.NOFOLLOW_LINKS)) ? true : false;
	}

	public static File create(String filePath) {
		return new File(filePath);
	}

	public static boolean isEmpty(String filePath) throws IOException{
		boolean isEmpty = false;
		
		try(FileInputStream fileInputStream = new FileInputStream(filePath)) {			
			isEmpty = (fileInputStream.getChannel().size() == 0) ? true : false;
		}
		return isEmpty;
	}
}
