package services;
import java.util.UUID;

public class IdGenerator {

	public String generateId() {
	UUID uniqueID = UUID.randomUUID();
    String randomUUIDString = uniqueID.toString();
	return randomUUIDString;
	}
}
