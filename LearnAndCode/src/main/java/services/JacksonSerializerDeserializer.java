package services;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import customexception.MyDbExceptions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class JacksonSerializerDeserializer implements SerializerDeserializerInterface {
	static ObjectMapper objectMapper = new ObjectMapper();
	
	public <T> void serialize(FileOutputStream outputStream, List<T> objectList) throws IOException {
		JsonGenerator JSONGenerator = objectMapper.getFactory().createGenerator(outputStream);
		objectMapper.writerWithDefaultPrettyPrinter().writeValue(JSONGenerator, objectList);
	}

	public <T> List<T> deserialize(FileInputStream fileInputStream, Class<T> classType) throws JsonParseException, IOException {
		// TODO Auto-generated method stub
		List<T> objectList;
		JsonParser JSONParser = objectMapper.getFactory().createParser(fileInputStream);
		objectList = objectMapper.readValue(JSONParser, objectMapper.getTypeFactory().constructCollectionType(List.class, classType));
		return objectList;
	}

}
