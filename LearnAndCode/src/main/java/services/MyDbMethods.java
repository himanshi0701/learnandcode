package services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import customexception.MyDbExceptions;

public class MyDbMethods {

	public static <T> void insert(T object) throws IOException {

		List<T> objectList = new ArrayList<T>();
		String filePath = FileUtil.getFilePath(object);

		if (!FileUtil.isPresent(filePath)) {
			FileUtil.create(filePath);
		} 
		else if (!FileUtil.isEmpty(filePath)) {
			objectList = (List<T>) ObjectManager.readObject(filePath, object.getClass());
		}
		System.out.println("I m here");
		ObjectManager.setId(object);
		objectList = ObjectManager.buildObjectList(objectList, object);
		ObjectManager.writeObject(filePath, objectList);
	}

	public static <T> List<T> search(String searchKey, String searchValue, T object)
			throws MyDbExceptions, IOException {

		String filePath = FileUtil.getFilePath(object);
		List<T> objectList = (List<T>) ObjectManager.readObject(filePath, object.getClass());
		return ObjectManager.getSearchedObjects(searchKey, searchValue, objectList);
	}


	public static <T> int delete(T object, String deleteKey, String deleteValue)
			throws IOException, MyDbExceptions {
		int rowsAffected = 0;
		String filePath = FileUtil.getFilePath(object);
		List<T> objectList = (List<T>) ObjectManager.readObject(filePath, object.getClass());

		List<T> objectsToDelete = MyDbMethods.search(deleteKey, deleteValue, object);
		rowsAffected = ObjectManager.removeObject(objectsToDelete.get(0), objectList);

		if (rowsAffected > 0)
			ObjectManager.writeObject(filePath, objectList);
		return rowsAffected;
	}


	public static <T> int update(T object, String id) throws IOException, MyDbExceptions {
		int rowsAffected = 0;
		String filePath = FileUtil.getFilePath(object);
		List<T> objectList = (List<T>) ObjectManager.readObject(filePath, object.getClass());
		rowsAffected = ObjectManager.setUpdatedObjectInList(id, object, objectList);
		if (rowsAffected > 0)
			ObjectManager.writeObject(filePath, objectList);
		return rowsAffected;
	}

}