package services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import customexception.MyDbExceptions;

public interface SerializerDeserializerInterface {
	public <T> void serialize(FileOutputStream outputStream, List<T> objectList)
			throws IOException;
	public <T> List<T> deserialize(FileInputStream fileInputStream, Class<T> classType)
			throws IOException, MyDbExceptions;
}


