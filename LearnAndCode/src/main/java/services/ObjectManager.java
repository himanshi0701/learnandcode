package services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;

import customexception.MyDbExceptions;

public class ObjectManager {
	static ObjectMapper objectMapper = new ObjectMapper();
	static JacksonSerializerDeserializer serializerDeserializer = new JacksonSerializerDeserializer();

	public static <T> void writeObject(String filePath, List<T> objectList) throws IOException {

		try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
			serializerDeserializer.serialize(outputStream, objectList);
		}
	}
	
	public static <T> List<T> readObject(String filePath, Class<T> classType) throws IOException {
		List<T> objectList;
		try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
			objectList = serializerDeserializer.deserialize(fileInputStream, classType);
		}
		return objectList;
	}
	
	public static <T> List<T> buildObjectList(List<T> objectList, T object) {
		objectList.add(object);
		return objectList;
	}
	
	public static <T> String getProperty(T object, String property) {
		String value = " ";
		try {
			value = object.getClass().getDeclaredField(property).get(object).toString();
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {

		}
		return value;
	}
	

	public static <T> void setId(T object) {
		IdGenerator idGenerator = new IdGenerator();
		String id = idGenerator.generateId();
		try {
			object.getClass().getDeclaredField("id").set(object, id);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {

		}
	}
	
	public static <T> List<T> getSearchedObjects(String searchKey, String searchValue, List<T> objectList)
			throws MyDbExceptions {
		List<T> searchedObjects = new ArrayList<>();
		for (T tempObject : objectList) {
			if (ObjectManager.getProperty(tempObject, searchKey).contains(searchValue)) {
				searchedObjects.add(tempObject);
			}
		}
		if (searchedObjects.isEmpty()) {
			throw new MyDbExceptions(007, "Object not found");
		}
		return searchedObjects;
	}
	
	public static <T> int setUpdatedObjectInList(String id, T object, List<T> objectList) {
		int rowsAffected = 0;
		for (int index = 0; index < objectList.size(); index++) {
			T tempObject = objectList.get(index);
			if (ObjectManager.getProperty(tempObject, "id").equals(new String(id).toString())) {
				ObjectManager.setId(object);
				objectList.set(index, object);
				rowsAffected += 1;
				break;
			}
		}
		return rowsAffected;
	}
	
	public static <T> int removeObject(T object, List<T> objectList) {
		int rowsAffected = 0;
		for (int index = 0; index < objectList.size(); index++) {
			T tempObject = objectList.get(index);
			if (ObjectManager.getProperty(tempObject, "id").equals(ObjectManager.getProperty(object, "id"))) {
				objectList.remove(index);
				rowsAffected += 1;
				break;
			}
		}
		return rowsAffected;
	}
}
