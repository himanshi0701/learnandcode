package customexception;

public class MyDbExceptions extends Exception{

	String errorMessage;
	int errorCode;
	
	public MyDbExceptions(int errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public void printMessage() {
		System.out.println(errorCode + ":" + errorMessage);
	}
}
