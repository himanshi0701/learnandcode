package mydb;

import static org.junit.jupiter.api.Assertions.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import customexception.MyDbExceptions;
import dbuser.Employee;
import dbuser.Student;
import mydb.MyDb;
import services.JacksonSerializerDeserializer;
import services.ObjectManager;


class MyDbTest {
	MyDb db = new MyDb();
	String randomUUIDString;
	Student student1;

	@BeforeEach public <T> void setObjectInFile() throws IOException {
		System.out.println("Executed");
		UUID uniqueID = UUID.randomUUID();
	    randomUUIDString = uniqueID.toString();
		student1 = new Student("Sam", "Delhi");
		student1.id = randomUUIDString;		
		String filePath = System.getProperty("user.dir") + "//ITTDB_FileStore//" + student1.getClass().getSimpleName() + ".json";
		File studentFile = new File(filePath);
		List<T> objectList = new ArrayList<T>();
		objectList = ObjectManager.buildObjectList(objectList,(T) student1);
		JacksonSerializerDeserializer serializer = new JacksonSerializerDeserializer();
		FileOutputStream outputStream = new FileOutputStream(filePath);
		serializer.serialize(outputStream, objectList);
		outputStream.close();
	}
	
	@AfterEach public void testTearDown() throws IOException {
		String filePath = System.getProperty("user.dir") + "//ITTDB_FileStore//Student.json";
		Files.deleteIfExists(Paths.get(filePath));
		System.out.println("TearDown Successfully Completed!!");
	}
	
	@Test
	void testSave() throws IOException, MyDbExceptions {
		String filePath = System.getProperty("user.dir") + "//ITTDB_FileStore//Employee.json";
		File file = new File(filePath);
		System.out.println("Executed");
		Employee employee1 = new Employee("Rita", "Software Engineer");
		db.save(employee1);
		assertTrue(file.exists());	
	}
	
	@Test
	void testDelete() throws IOException, MyDbExceptions {
		System.out.println("Executed");
		int rowsAffected = db.delete(student1, "id", randomUUIDString);
		assertEquals(rowsAffected,1);	
	}
	
	@Test
	void testUpdate() throws IOException, MyDbExceptions {
		student1.setName("Himanshi");
		student1.setAddress("India");
		int rowsUpdated = db.modify(student1,randomUUIDString);
		assertEquals(rowsUpdated,1);
	}
	
	@Test
	void testSearch() throws IOException, MyDbExceptions {
		System.out.println("Executed");
		List<Student> employee = db.find(student1, "name", "Sam");
		assertEquals(employee.size(),1);
	}

}
